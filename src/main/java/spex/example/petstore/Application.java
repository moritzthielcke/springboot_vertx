/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spex.example.petstore;
import io.vertx.core.Vertx;
import javax.annotation.PostConstruct;
import spex.core.cmp.SpexVerticle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

/**
 *
 * @author moritz
 */
@SpringBootApplication
@ComponentScan(basePackages = {"spex.core"})
public class Application {
    private static final Logger logger = LoggerFactory.getLogger(Application.class);	
    private final Vertx vertx;
    
    @Autowired
    private SpexVerticle spex;   
    
    
    public static void main(String[] args) {
       SpringApplication.run(Application.class);
    }

    public Application(){
       vertx = Vertx.vertx();
    }
    
    @PostConstruct
    public void deployServerVerticle() {
      vertx.deployVerticle(spex);
    }  
    
    @Bean
    public Vertx vertx(){
        return vertx;
    }
  
}
