/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spex.example.petstore.handler;

import io.vertx.core.Handler;
import io.vertx.ext.web.RoutingContext;
import spex.core.annotations.GlobalHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author moritz
 */
@GlobalHandler
public class EnableCors implements Handler<RoutingContext>{
    private static final Logger logger = LoggerFactory.getLogger(EnableCors.class);	
    
    @Override
    public void handle(RoutingContext e) {
        logger.info("hi from global handler ! "+e.request().path());
        e.next();
    }
    
}
