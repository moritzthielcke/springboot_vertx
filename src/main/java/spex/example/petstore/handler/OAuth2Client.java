/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spex.example.petstore.handler;

import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.auth.User;
import io.vertx.ext.auth.oauth2.AccessToken;
import io.vertx.ext.auth.oauth2.OAuth2Auth;
import io.vertx.ext.auth.oauth2.OAuth2ClientOptions;
import io.vertx.ext.auth.oauth2.OAuth2FlowType;
import io.vertx.ext.web.RoutingContext;
import spex.core.annotations.Operation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author moritz
 */
@Operation(name = "oauthclient")
public class OAuth2Client implements Handler<RoutingContext>{
    private static final Logger logger = LoggerFactory.getLogger(OAuth2Client.class);	

    
    @Override
    public void handle(RoutingContext ctx) {
        logger.info("oauth client login "+ctx.request().getParam("client")+"@"+ctx.request().getParam("site"));
        OAuth2ClientOptions options = new OAuth2ClientOptions()
                                            .setClientID(ctx.request().getParam("client"))
                                            .setClientSecret(ctx.request().getParam("secret"))
                                            .setSite(ctx.request().getParam("site"));
        OAuth2Auth auth = OAuth2Auth.create(ctx.vertx(), OAuth2FlowType.CLIENT, options);
        auth.authenticate(new JsonObject(), (res) -> {
            if(res.failed()){
                logger.error("access token errror : "+res.cause().getMessage());
                ctx.fail(500);
            }
            else{
                ctx.response().end(res.result().toString());
            }
        });
    }
    
}
