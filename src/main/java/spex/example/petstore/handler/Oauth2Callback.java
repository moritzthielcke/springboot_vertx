/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spex.example.petstore.handler;

import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.client.WebClient;
import spex.core.annotations.Operation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author moritz
 */
@Operation(name = "oauth2Callback")
public class Oauth2Callback implements Handler<RoutingContext>{
    private static final Logger logger = LoggerFactory.getLogger(Oauth2Callback.class);	

    @Autowired
    private WebClient client;
 
    @Override
    public void handle(RoutingContext ctx) {
        String code = ctx.request().params().get("code");
        logger.info("code => "+code);
        ctx.response().end(code);
    }
    
}
