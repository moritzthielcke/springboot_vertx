/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spex.example.petstore.handler;

import io.vertx.core.Handler;
import io.vertx.ext.web.RoutingContext;
import spex.core.annotations.SecurityHandler;
import spex.core.cmp.auth.JWTAuthImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author moritz
 */
@SecurityHandler(schema = "BearerAuth")
public class BearerAuth implements Handler<RoutingContext>{
    private static final Logger logger = LoggerFactory.getLogger(BearerAuth.class);	

    @Autowired
    JWTAuthImpl jwtAuth;

    @Override
    public void handle(RoutingContext e) {
        logger.info("hi from bearer auth");
        jwtAuth.authHandler().handle(e);
        //e.next();
    }
    
}
