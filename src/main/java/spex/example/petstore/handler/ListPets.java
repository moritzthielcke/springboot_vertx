/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spex.example.petstore.handler;

import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.ext.web.RoutingContext;
import spex.core.annotations.Operation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author moritz
 */
@Operation(name = "listPets")
public class ListPets implements Handler<RoutingContext>{
    private static final Logger logger = LoggerFactory.getLogger(ListPets.class);	

    Integer count=0;
    
    @Override
    public void handle(RoutingContext e) {
        String username = (e.user() != null) ? e.user().principal().toString() : "unkown";
        e.response().end("hi "+username+" from listpets");
        if(e.user()!=null){
            e.user().isAuthorized("admin", (AsyncResult<Boolean> e1) -> {
                logger.info(e.user().principal().toString()+" is admin ? "+e1.result());
            });
        }
        logger.info("Listpet was called "+(++count)+" times");
    }
    
}
