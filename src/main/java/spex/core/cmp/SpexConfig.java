/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spex.core.cmp;

import java.util.Objects;
import java.util.stream.Stream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

/**
 *
 * @author moritz
 */
@Configuration
public class SpexConfig {  
    @Autowired
    private Environment environment;
    
    private String applicationName;
    private Integer httpPort;
    
    public String apiImplementation(){
        return environment.getProperty("spex.api.impl");
    }
    
    public String apiSpecification(){
        return environment.getProperty("spex.api.spec");
    }
    
    public int apiPort(){
        Integer apiPort = environment.getProperty("spex.api.port", Integer.class);
        if(apiPort==null){
            apiPort = 8081;
        }
        return apiPort;
    }
    
    
    public String applicationName() {
        if(applicationName == null){
            applicationName = Stream.of( environment.getProperty("spex.application.name"), 
                                         environment.getProperty("spring.application.name") )
                                .filter((String t) -> {
                                    return (t != null && !t.isEmpty() );
                                })
                                .findFirst()
                                .orElse("unknown");
}
        return this.applicationName;
   }
   
   public int httpPort() {
       if(httpPort == null){
           httpPort = Stream.of( environment.getProperty("spex.http.port", Integer.class),
                             environment.getProperty("server.port", Integer.class) )
                    .filter(Objects::nonNull)
                    .findFirst()
                    .orElse(8080);
       }
       return httpPort;
   }


}
