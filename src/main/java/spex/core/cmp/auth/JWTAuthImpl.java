/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spex.core.cmp.auth;

import io.vertx.core.Vertx;
import io.vertx.ext.auth.PubSecKeyOptions;
import io.vertx.ext.auth.jwt.JWTAuth;
import io.vertx.ext.auth.jwt.JWTAuthOptions;
import io.vertx.ext.web.handler.JWTAuthHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

/**
 *
 * @author moritz
 */
@Component
public class JWTAuthImpl {
    private static final Logger logger = LoggerFactory.getLogger(JWTAuthImpl.class);	
    private JWTAuth authProvider;
    private JWTAuthHandler authHandler;
    
    @Autowired
    private Environment environment; //@todo use config object for configuration
    @Autowired
    private Vertx vertx;    
    
    public JWTAuth authProvider(){
        if(authProvider==null){
            JWTAuthOptions config = new JWTAuthOptions()
                                    .addPubSecKey( new PubSecKeyOptions()
                                                   .setAlgorithm( environment.getProperty("spex.api.jwt.algo") )
                                                   .setPublicKey( environment.getProperty("spex.api.jwt.pubkey") ) );
            authProvider = JWTAuth.create(vertx, config);
        }
        return authProvider;
    }

    public JWTAuthHandler authHandler(){
        if(authHandler==null){
            authHandler = JWTAuthHandler.create(authProvider());
        }
        return authHandler;
    }
    
}
