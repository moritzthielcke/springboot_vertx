/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spex.core.cmp;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Handler;
import io.vertx.core.http.HttpServerOptions;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.api.contract.openapi3.OpenAPI3RouterFactory;
import io.vertx.ext.web.handler.StaticHandler;
import spex.core.annotations.GlobalHandler;
import spex.core.annotations.Operation;
import spex.core.annotations.SecurityHandler;
import org.reflections.Reflections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

/**
 *
 * @author moritz
 */
@Component
public class SpexVerticle extends AbstractVerticle{
    private static final Logger logger = LoggerFactory.getLogger(SpexVerticle.class);	
    
    @Autowired
    private SpexConfig config;
    @Autowired
    private ApplicationContext appContext;
    @Autowired
    private AutowireCapableBeanFactory beanFactory;


    
    @Override
    public void start() throws Exception {
        super.start();
        //start open api http server, @todo add servercount to config
        initOpenAPIServer(new HttpServerOptions().setPort(config.apiPort()), 
                          config.apiSpecification(),
                          config.apiImplementation()
        );
        //just a plain http server for static file serving and other things that arent part of the api
        initHttpServer(new HttpServerOptions().setPort(config.httpPort()));
    }

    
    private void initHttpServer(HttpServerOptions serverOptions) {
        Router router = Router.router(vertx);
        router.route("/info").handler(routingContext -> {
            HttpServerResponse response = routingContext.response();
            response.putHeader("Content-Type", "application/json");
            response.end("hello "+config.applicationName());
            //routingContext.next();
        });
        /*.handler((e) -> {
            logger.info("test"+e.request());
        });*/        
        router.route("/static/*").handler(StaticHandler.create().setWebRoot("static")::handle);
        router.route("/liveness").handler((e) -> {
            e.response().end("OK");
        });
        router.route("/readiness").handler((e) -> {
            e.response().end("OK");
        });
        vertx.createHttpServer().requestHandler(router::accept).listen(serverOptions.getPort());
        logger.info("http server listening on port "+serverOptions.getPort());
    }   
    
        
    private void initOpenAPIServer(HttpServerOptions serverOptions, String schema, String classpath){
        if(schema==null){
            return;
        }
        logger.info("loading OpenAPI schema from "+schema);
        OpenAPI3RouterFactory.create(vertx, schema, ar -> {
            if (ar.succeeded()) {
                logger.info("OpenAPI schema loaded successfully");
                OpenAPI3RouterFactory routerFactory = ar.result();                   
                Reflections handlerReflections = new Reflections(classpath); //@todo maybe spring uses something else for teflections
                try{
                    mountGlobalHandler(routerFactory, handlerReflections);
                    mountSecurityHandler(routerFactory, handlerReflections);
                    mountAPIOperations(routerFactory, handlerReflections);
                }catch(RuntimeException ex){
                    logger.error("cant load api implementation",ex);
                    vertx.close();
                    SpringApplication.exit(appContext, () -> -1);
                    return;
                }
                vertx.createHttpServer(serverOptions).requestHandler(routerFactory.getRouter()::accept).listen();
                logger.info("OpenAPI listening on "+serverOptions.getPort());
            } else {
              // Something went wrong during router factory initialization
              logger.error("cant read API spec", ar.cause());
            }
        });           
    }
    
    public void mountGlobalHandler(OpenAPI3RouterFactory factory, Reflections handlerReflections){
        logger.info("loading global handlers");
        for (Class c : handlerReflections.getTypesAnnotatedWith(GlobalHandler.class)) {
            factory.getOptions().addGlobalHandler((Handler) beanFactory.createBean(c));
            logger.info(" global handler : "+c.getName());
        }
    }

    public void mountAPIOperations(OpenAPI3RouterFactory factory, Reflections handlerReflections){
        logger.info("loading api operations");
        for (Class c : handlerReflections.getTypesAnnotatedWith(Operation.class)) { 
            Operation op = (Operation) c.getAnnotation(Operation.class); 
            factory.addHandlerByOperationId(op.name(), (Handler)beanFactory.createBean(c));
            logger.info(" api operation : "+op.name()+" => "+c.getName());
        }       
    }
    
    public void mountSecurityHandler(OpenAPI3RouterFactory factory, Reflections handlerReflections){
        logger.info("loading securtiy handler");
        for (Class c : handlerReflections.getTypesAnnotatedWith(SecurityHandler.class)) { 
            SecurityHandler sec = (SecurityHandler) c.getAnnotation(SecurityHandler.class); 
            factory.addSecurityHandler(sec.schema(), (Handler) beanFactory.createBean(c));
            logger.info(" security handler : "+sec.schema()+" -> "+c.getName());
        }
    }

    
   
}
