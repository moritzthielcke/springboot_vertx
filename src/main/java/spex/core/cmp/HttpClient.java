/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spex.core.cmp;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpServerOptions;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.api.contract.openapi3.OpenAPI3RouterFactory;
import io.vertx.ext.web.client.WebClient;
import spex.core.annotations.GlobalHandler;
import spex.core.annotations.Operation;
import spex.core.annotations.SecurityHandler;
import org.reflections.Reflections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

/**
 *
 * @author moritz
 */
@Component
public class HttpClient{
    private static final Logger logger = LoggerFactory.getLogger(HttpClient.class);	
    private WebClient client;
    
    @Autowired
    private SpexConfig config;
    @Autowired
    private ApplicationContext appContext;
    @Autowired
    private Vertx vertx;

    @Bean
    public WebClient getWebClient(){
        if(client == null){
            client = WebClient.create(vertx);
        }
        return client;
    }

}
