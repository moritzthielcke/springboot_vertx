#!/bin/bash +x 

# config
IMAGE='spex/petstore'
REGISTRY='127.0.0.1:30400'
VERSION=$(date '+%Y%m%d%H%M%S')

#build local image
cp target/*.jar target/deploy/app/app.jar
cp petstore.yaml target/deploy/app/.

docker build -t "${IMAGE}" -f target/deploy/Dockerfile target/deploy/



# push / push as latest / apply to kubernetes ?

while [ "$#" -gt "0" ]
do
	if [ "$1" == "push" ] ; then
		echo ">> pushing as ${REGISTRY}/${IMAGE}:${VERSION}"
		docker tag "${IMAGE}" "${REGISTRY}/${IMAGE}:${VERSION}"
		docker push "${REGISTRY}/${IMAGE}:${VERSION}"	
	fi
	if [ "$1" == "latest" ] ; then
		echo ">> pushing as ${REGISTRY}/${IMAGE}:latest"
		docker tag "${IMAGE}" "${REGISTRY}/${IMAGE}:latest"
		docker push "${REGISTRY}/${IMAGE}:latest"
	fi	
	if [ "$1" == "apply" ] ; then
		sed "s/:latest/:${VERSION}/" target/deploy/k8.yml > "target/deploy/k8-${VERSION}.yml"
		kubectl apply -f target/deploy/k8-${VERSION}.yml
	fi		
  	shift
done              

