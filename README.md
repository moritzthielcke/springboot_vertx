# About #

Spring goes Vertx example, including the vertx-web-api-contract module.

There is a much cleaner und newer version of this project under:
https://bitbucket.org/moritzthielcke/spex
but its not as complete as this one.






### Authentication ###

If you use the local version of petstore.yaml, some endpoinds will require authentication.
You can generate your own token under : https://jwt.io/ , i only tested RS256 so far.

Here is a token working with the pubkey provided in the application.properties:
Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWUsImlhdCI6MTUxNjIzOTAyMn0.TCYt5XsITJX1CxPCT8yAV-TVkIEq_PbChOMqsLfRoPsnsgw5WEuts01mq-pQy7UJiN5mgRxD-WUcX16dUEMGlv50aqzpqh4Qktb3rk-BuQy72IFLOqV0G_zS245-kronKb78cPN25DGlcTwLtjPAYuNzVBAh4vGHSrQyHUdBBPM



### Links ###

https://vertx.io/docs/vertx-web-api-contract/java/


https://vertx.io/

https://spring.io/projects/spring-boot
